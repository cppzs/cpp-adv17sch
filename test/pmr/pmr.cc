// testing polymorphic allocator
/*
 * Copyright (c) 2017-2020 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <memory>
#include <iostream>
#include <list>
#include <vector>

#include <memory_resource>

using std::cout;
using std::pmr::list;
using std::pmr::vector;

typedef std::pmr::monotonic_buffer_resource mbr;

int main()
{
    constexpr size_t bufSize = 1024*1024;
    auto buf1 = new std::byte[bufSize];
    mbr alloc1{buf1, bufSize};

    cout << "test vector\n";
    vector<int> vi(5, 1, &alloc1);
    for (int i = 5; i != 2000; ++i)
    {
        vi.push_back(i);
    }

    auto buf2 = new std::byte[bufSize];
    mbr alloc2{buf2, bufSize};

    cout << "test list\n";
    list<int> li(5, 1, &alloc2);
    for (int i = 5; i != 10; ++i)
    {
        li.push_back(i);
    }

    return 0;
}
