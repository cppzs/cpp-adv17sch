// testing parallel algorithms
/*
 * Copyright (c) 2014-2020 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <iostream>
#include <vector>

#include <algorithm>
#include <numeric>

#include <execution>


int main()
{
    constexpr size_t vecSize = 1'000'000;
    std::vector<double> nums(vecSize);
    std::iota(nums.begin(), nums.end(), 0);

    auto result
        = std::transform_reduce(std::execution::seq
                                , nums.begin(), nums.end()
                                , 0.0
                                , [] (auto v1, auto v2) { return v1 + v2; }
                                , [] (auto val) { return val*val; });
    std::cout << "some elem: " << nums[3] << '\n';
    std::cout << "Result: " << result << '\n';

    return 0;
}
