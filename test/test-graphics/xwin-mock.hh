/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
// we use the same include guard as the real file
#ifndef XWIN_HH_SEEN_
#define XWIN_HH_SEEN_
#include "trompeloeil.hpp"
#include "graphmock.hh"
#include "test-classes.hh"

#include <functional>

namespace exerciseTest
{

extern Sequencer<GuiFuncs> *globalGuiSeq;

struct XWinWrap;

class XWinMock
{
public:
    XWinMock();
    ~XWinMock();

    MAKE_MOCK1(create, void(XWinWrap const *));
    MAKE_MOCK1(clean, void(XWinWrap const *));
    MAKE_MOCK0(getSurface, cairo_surface_t *());
    MAKE_MOCK2(registerCb, void(XWinWrap const *, std::function<void()>));
    MAKE_MOCK1(unregisterCb, void(XWinWrap const *));

private:
    XWinMock *oldMock = nullptr;
};
typedef XWinMock GuiWinMock;

struct XWinTestHelper
{
    ~XWinTestHelper();
    void seq(GuiFuncs f);

    //void setGuiFuncSeq(Sequencer<GuiFuncs> *s);

    XWinWrap *host = nullptr;
    Sequencer<GuiFuncs> *seqObj = nullptr;
    std::function<void()> cb;
    bool wrongRegisterCbOnFull = false;
    bool wrongUnregCbOnEmpty = false;
};

struct XWinWrap
{
public:
    XWinWrap(int, int, std::string const &);
    XWinWrap(XWinWrap &&rhs);
    ~XWinWrap() noexcept(false);
    XWinWrap &operator=(XWinWrap &&rhs);

    cairo_surface_t *getSurface();
    void show() const {}
    void destroy() {}
    void loop() const {}

    template <class FuncT, class ArgT>
    void registerCallback(FuncT f, ArgT a);
    void registerCallback(std::function<void()> f);
    void unregisterCallback();

    XWinWrap const *getMe() const;

    static XWinMock *mock;
    static CairoWrap *cairoWrapPtr;
    static XWinTestHelper *helperPtr;
    static XWinWrap *curWin;

    XWinTestHelper *myHelper = nullptr;
    CairoWrap *myCairo = nullptr;
    bool valid{true};
};
typedef XWinWrap GuiWinWrap;

template <class FuncT, class ArgT>
void XWinWrap::registerCallback(FuncT f, ArgT a)
{
    registerCallback([f,a] { f(a); });
}
} // namespace exerciseTest

namespace exercise
{
typedef exerciseTest::XWinWrap GuiWin;
} // namespace exercise
#endif /* XWIN_HH_SEEN_ */
