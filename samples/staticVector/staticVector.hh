// resource management with copy
/*
 * Copyright (c) 2014-2015 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CPP_SAMPLE_STATIC_VECTOR_HH
#define CPP_SAMPLE_STATIC_VECTOR_HH

#include <cstddef>

namespace cppSample
{
template <class T>
class StaticVector
{
public:
    explicit StaticVector(int size);
    StaticVector(StaticVector &&) noexcept;
    StaticVector(StaticVector const &);
    ~StaticVector();

    StaticVector &operator=(StaticVector);

    T operator[](int i);

    T *begin();
    T const *begin() const;
    T *end();
    T const *end() const;

private:
    size_t dataSize;
    T *data;
};
} // namespace cppSample
#endif /* CPP_SAMPLE_STATIC_VECTOR_HH */
