// testing StaticVector

#include "staticVector.hh"

#include <algorithm>
#include <iterator>
#include <iostream>

#include "staticVector.tt"

namespace
{
typedef cppSample::StaticVector<int> IntSV;

void print(IntSV const &v)
{
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';
}
} // unnamed namespace

int main()
{

    IntSV iv1(15);
    std::fill(iv1.begin(), iv1.end(), 12);

    IntSV iv2(iv1);
    std::fill(iv1.begin(), iv1.end(), 22);

    IntSV iv3(25);
    std::fill(iv3.begin(), iv3.end(), 1);

    print(iv3);

    iv3 = iv1;
    print(iv1);
    print(iv2);
    print(iv3);

    iv3 = std::move(iv1);
    print(iv1);
    print(iv2);
    print(iv3);

    return 0;
}
