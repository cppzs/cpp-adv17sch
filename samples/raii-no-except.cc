// example for RAII w/o exceptions
#include <memory>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>

namespace TestUtils
{
// create OpenSSL test certificate
std::unique_ptr<X509, void(*)(X509 *)> makeTestCert(char const *cn)
{
    std::unique_ptr<X509, void(*)(X509 *)> cert{X509_new(), X509_free};
    std::unique_ptr<X509, void(*)(X509 *)> noCert{nullptr, X509_free};
    if (!cert)
    {
        return cert;
    }

    // subject
    std::unique_ptr<X509_NAME, void(*)(X509_NAME *)> subj{
        X509_NAME_new(), X509_NAME_free};
    if (!subj)
    {
        return noCert;
    }
    if (cn)
    {
        auto val = reinterpret_cast<unsigned char const *>(cn);
        int status = X509_NAME_add_entry_by_txt(subj.get(), "CN"
                                                , MBSTRING_ASC, val
                                                , -1, -1, 0);
        if (status != 1)
        {
            return noCert;
        }
    }
    int status = X509_set_subject_name(cert.get(), subj.get());
    if (status != 1)
    {
        return noCert;
    }

    // public key
    EVP_PKEY *key = nullptr;
    std::unique_ptr<EVP_PKEY_CTX, void(*)(EVP_PKEY_CTX *)> kCtx{
        EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, nullptr), EVP_PKEY_CTX_free};
    if (!kCtx)
    {
        return noCert;
    }
    if (EVP_PKEY_keygen_init(kCtx.get()) != 1)
    {
        return noCert;
    }
    if (EVP_PKEY_keygen(kCtx.get(), &key) != 1)
    {
        return noCert;
    }
    if (X509_set_pubkey(cert.get(), key) != 1)
    {
        return noCert;
    }

    return cert;
}
} // namespace TestUtils
