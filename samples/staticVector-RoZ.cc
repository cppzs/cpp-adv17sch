// resource management with unique_ptr
/*
 * Copyright (c) 2014-2015 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cstddef>
#include <memory>

#include <algorithm>
#include <iterator>
#include <iostream>
#include <utility>

namespace cppSample
{
template <class T>
class StaticVector
{
public:
    explicit StaticVector(int size);
    StaticVector(StaticVector &&) noexcept = default;
    StaticVector(StaticVector const &) = delete;
    ~StaticVector() = default;

    StaticVector &operator=(StaticVector &&) = default;
    StaticVector &operator=(StaticVector const &) = delete;

    T operator[](int i);

    T *begin();
    T const *begin() const;
    T *end();
    T const *end() const;

private:
    size_t dataSize;
    std::unique_ptr<T[]>data;
};


template <typename T>
StaticVector<T>::StaticVector(int size)
  : dataSize(size)
  , data(new T[size])

{
}

template <typename T>
T StaticVector<T>::operator[](int i)
{
    return (data.get())[i];  // UB if moved from, ok
}

template <typename T>
T const *StaticVector<T>::begin() const
{
    return data.get();     // nullptr if moved from, ok
}

template <typename T>
T *StaticVector<T>::begin()
{
    return data.get();     // nullptr if moved from, ok
}

template <typename T>
T const *StaticVector<T>::end() const
{
    // default move doesn't set dataSize to 0
    if (data)
    {
        return data.get() + dataSize;
    } else {
        return nullptr;
    }
}

template <typename T>
T *StaticVector<T>::end()
{
    // default move doesn't set dataSize to 0
    if (data)
    {
        return data.get() + dataSize;
    } else {
        return nullptr;
    }
}
} // namespace cppSample


namespace
{
typedef cppSample::StaticVector<int> IntSV;

void print(IntSV const &v)
{
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';
}
} // unnamed namespace

int main()
{

    IntSV iv1(15);
    std::fill(iv1.begin(), iv1.end(), 12);

    IntSV iv2(std::move(iv1));
    std::fill(iv1.begin(), iv1.end(), 22);

    IntSV iv3(25);
    std::fill(iv3.begin(), iv3.end(), 1);

    print(iv3);

    iv3 = std::move(iv1);
    print(iv1);
    print(iv2);
    print(iv3);

    return 0;
}
