// finding words in files, try #3

/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string_view>
#include <string>
#include <exception>
#include <system_error>

using std::string_view;
using std::system_error;
using std::exception;
using std::cerr;


namespace fs=std::filesystem;

void searchWord(fs::path fname, string_view sWord)
{
    try
    {
        std::ifstream f(fname);

        std::string line;
        while (std::getline(f, line))
        {
            if (line.find(sWord) != line.npos)
            {
                std::cout << "found in " << fname.string() << '\n';
                break;
            }
        }
    }
    catch (system_error &err) { cerr << err.code().message() << '\n'; }
    catch (exception &err) { cerr << err.what() << '\n'; }
}

class SearchFiles
{
public:
    SearchFiles(string_view directory) : dir(directory) {}

    void operator()(string_view sWord);

private:
    fs::path dir;
};

void SearchFiles::operator()(string_view sWord)
{
    try
    {
        if (fs::exists(dir) && fs::is_directory(dir))
        {
            fs::recursive_directory_iterator end;
            for (fs::recursive_directory_iterator i(dir); i != end; ++i)
            {
                if (fs::is_regular_file(*i))
                {
                    searchWord(*i, sWord);
                }
            }
        }
        else
        {
            cerr << "no such directory" << '\n';
        }
    }
    catch (system_error &err) { cerr << err.code().message() << '\n'; }
    catch (exception &err) { cerr << err.what() << '\n'; }
}



int main(int argc, char *argv[])
{
    try
    {
        SearchFiles sf(argv[2]);
        sf(argv[1]);
    }
    catch (system_error &err) { cerr << err.code().message() << '\n'; }
    catch (exception &err) { cerr << err.what() << '\n'; }
	
    return 0;
}
