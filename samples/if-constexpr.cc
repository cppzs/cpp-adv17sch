// example from sfinae.cc simpler with enable_if

#include <iostream>
#include <type_traits>

using std::cout;

void f1(int i)
{
    cout << "f(" << i << ")\n";
}
void f2(int *p)
{
    cout << "f(" << p << ")\n";
}

// goal: a single function that calls f1() for int and f2() for int*

template <typename T>
void f(T val)
{
    if constexpr (std::is_integral_v<T>)
    {
        f1(val);
    } else {
        f2(val);
    }
}

template <typename T>
bool DependentAlwaysFalse_v = false;

template <typename T>
constexpr bool AlwaysReturnFalse()
{
    return false;
}
// the message to static_assert must be a string literal,
// so this needs to be a macro
#if 1
#define MY_ALWAYS_ASSERT(msg) \
    static_assert(AlwaysReturnFalse<int>(), msg)
#else
#define MY_ALWAYS_ASSERT(msg) \
    static_assert(true, msg)
#endif

void g()
{
    if constexpr (sizeof(int) < 4)
    {
        cout << "version for small systems\n";
    } else if constexpr (sizeof(int) == 4)
    {
        cout << "version for 32-bit systems\n";
    } else
    {
        // this wouldn't work, as it's always evaluated (at compile time)
        //static_assert(false, "Only works for 32-bit int or smaller");
    }
    // in a non-template, we need to specify the combined conditions here
    static_assert(sizeof(int) <= 4, "Only works for 32-bit int or smaller");
}

template <typename T>
void h()
{
    if constexpr (sizeof(T) < 4)
    {
        cout << "version for small systems\n";
    } else if constexpr (sizeof(T) == 4)
    {
        cout << "version for 32-bit systems\n";
    } else
    {
        // this wouldn't work, as it's always evaluated (at compile time)
        //static_assert(false, "Only works for 32-bit int or smaller");
        // in a template we can work around this
        // this works as there might (in theory) be specializations
        // for which DependentAlwaysFalse_v is true
        static_assert(DependentAlwaysFalse_v<T>,
                      "Only works for 32-bit int or smaller");
    }
}


int main()
{
    int i = 5;
    f(i);
    f(&i);

    g();

    h<int>();

    return 0;
}
